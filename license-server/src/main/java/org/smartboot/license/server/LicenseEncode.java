/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseEncode.java
 * Date: 2020-03-26
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.server;

import org.smartboot.license.client.LicenseEntity;
import org.smartboot.license.client.LicenseException;
import org.smartboot.license.client.Md5;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Date;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/26
 */
public class LicenseEncode {
    private static final int MAX_DATA_LENGTH = 64;
    private static final int MIN_DATA_LENGTH = 32;
    private final byte[] cacheByte = new byte[8];

    public byte[] encode(LicenseEntity entity, byte[] privateKey) throws IOException {
        check(entity);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        //魔数
        byteArrayOutputStream.write(LicenseEntity.MAGIC_NUM);
        //申请时间
        writeLong(entity.getApplyTime(), byteArrayOutputStream);
        System.out.println("apply time: " + new Date(entity.getApplyTime()));
        // 过期时间
        writeLong(entity.getExpireTime(), byteArrayOutputStream);
        System.out.println("expire time: " + new Date(entity.getExpireTime()));
        //md5
        byte[] md5 = Md5.md5(entity.getData()).getBytes();
        writeInt(md5.length, byteArrayOutputStream);
        byteArrayOutputStream.write(md5);
        System.out.println("md5: " + Md5.md5(entity.getData()));

        //公钥
        writeInt(entity.getPublicKeys().length, byteArrayOutputStream);
        byteArrayOutputStream.write(entity.getPublicKeys());
        System.out.println("publicKey: " + Base64.getEncoder().encodeToString(entity.getPublicKeys()));

        // 申请者
        byte[] applicant = entity.getApplicant().getBytes();
        writeInt(applicant.length, byteArrayOutputStream);
        byteArrayOutputStream.write(applicant);
        System.out.println("applicant: " + entity.getApplicant());

        //联系方式
        byte[] contact = entity.getContact().getBytes();
        writeInt(contact.length, byteArrayOutputStream);
        byteArrayOutputStream.write(contact);
        System.out.println("contact: " + entity.getContact());

        int offset = 0;
        int step = nextStep();
        byte[] data = entity.getData();
        if (data.length < step) {
            step = data.length;
        }
        while (offset < data.length) {
            byte[] encryptData = RasUtil.encryptByPrivateKey(data, privateKey, offset, step);
            byteArrayOutputStream.write(encryptData.length);
            byteArrayOutputStream.write(encryptData);
            writeLong(entity.getExpireTime() % step, byteArrayOutputStream);
            offset += step;
            int nextStep = nextStep();
            step = Math.min(data.length - offset, nextStep);
        }
        byteArrayOutputStream.write(0);
        return byteArrayOutputStream.toByteArray();
    }

    private int nextStep() {
        int step = (int) (System.nanoTime() % MAX_DATA_LENGTH);
        if (step < MIN_DATA_LENGTH) {
            step = MIN_DATA_LENGTH;
        }
        return step;
    }

    private void writeLong(long v, OutputStream outputStream) throws IOException {
        cacheByte[0] = (byte) (v >>> 56);
        cacheByte[1] = (byte) (v >>> 48);
        cacheByte[2] = (byte) (v >>> 40);
        cacheByte[3] = (byte) (v >>> 32);
        cacheByte[4] = (byte) (v >>> 24);
        cacheByte[5] = (byte) (v >>> 16);
        cacheByte[6] = (byte) (v >>> 8);
        cacheByte[7] = (byte) (v >>> 0);
        outputStream.write(cacheByte, 0, 8);
    }

    private void writeInt(int v, OutputStream outputStream) throws IOException {
        cacheByte[0] = (byte) (v >>> 24);
        cacheByte[1] = (byte) (v >>> 16);
        cacheByte[2] = (byte) (v >>> 8);
        cacheByte[3] = (byte) (v >>> 0);
        outputStream.write(cacheByte, 0, 4);
    }

    private void check(LicenseEntity entity) {
        if (entity.getExpireTime() < System.currentTimeMillis()) {
            throw new LicenseException("invalid expire time");
        }
        if (entity.getPublicKeys() == null) {
            throw new LicenseException("invalid publicKey");
        }
        if (entity.getData() == null) {
            throw new LicenseException("invalid license data");
        }
    }
}
