package org.smartboot.license.server;

import org.smartboot.license.client.LicenseEntity;

import java.security.KeyPair;
import java.util.Base64;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2022/11/13
 */
public class LicenseEncodeTest {
    public static void main(String[] args) throws Exception {
        LicenseEncode encode = new LicenseEncode();
        KeyPair keyPair = RasUtil.initKey();
        LicenseEntity entity = new LicenseEntity(System.currentTimeMillis() + 60 * 1000, RasUtil.getPublicKey(keyPair));
        entity.setApplicant("三刀的朋友圈");
        entity.setContact("123");
        entity.setData("Hello World".getBytes());
        byte[] bytes = encode.encode(entity, RasUtil.getPrivateKey(keyPair));
        System.out.println(Base64.getEncoder().encodeToString(bytes));
    }
}
