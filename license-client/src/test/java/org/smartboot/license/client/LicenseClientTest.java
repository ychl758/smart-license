/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseTest.java
 * Date: 2020-03-22
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.client;

import java.io.IOException;
import java.util.Base64;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/21
 */
public class LicenseClientTest {
    public static void main(String[] args) throws IOException {
        License license = new License();
        LicenseEntity licenseData = license.loadLicense(Base64.getDecoder().decode("c21hcnQtbGljZW5zZQAAAYRw5jkAAAABhHDnI2AAAAAgYjEwYThkYjE2NGUwNzU0MTA1YjdhOTliZTcyZTNmZTUAAACiMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJi1nSXXoYUcwCFfnvj9IdLlPo0jLWUa7XcdoDQJPwfyp0bef/j8LERksu1s1HQC/T4mtuobDasBLqGmUqBiFQrBjkYSHIjtPEN0hI+XKfWw8wM+FobYPHjMCuKKK5ar6zE1YdA7EUOndLNXO/lvtBEFzvNagoybsgI8tpB7v1+QIDAQABAAAAEuS4ieWIgOeahOaci+WPi+WciAAAAAMxMjOANxb00BoJ93tAigyIdAnCZ9UPX2jaLJ4Uqd595lUHhbL8OT6s9HCdqY3ElKTyx76wWlYaIraHYG9ttKLjEogpvH3cHv4Mv/HG8DM4cKh+rju4O4Hzy2+4vHFxOcpc/WhqAcoDYIIAEkBcYb30Y2C5rn8kUi/KwOAqpHf9t9KuLlQAAAAAAAAAAwA="));
        System.out.println(new String(licenseData.getData()));
    }
}
